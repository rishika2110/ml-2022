======================================================================================
======================================================================================
    This folder contains files to run the Convolutional Language Modelling task  
======================================================================================



======================================================================================
Important Contents:
======================================================================================
1. main_2.py: File without wandb and residual connection network.
2. main_wandb.py: File with wandb but no residual connection network.
3. main_rc.py: File with wandb and residual connection network.
4. config: File needed to run wandb.
5. datautils: File contains data preprocessing steps.
6. model.py: No residual connection CNN model.
7. model_.py: CNN model along with residual connections.
======================================================================================



======================================================================================
Running scripts:
======================================================================================
Before running any experiements please change the directory using the command:

***********
cd Conv_LM
***********

To run directly non residual connection network run the following command 
in the command line: 
*****************
python main_2.py 
******************

To integrate wandb for hyper-parameter search for non residual connection network,
run the following command in the command line: 
********************
python main_wandb.py
********************

To integrate wandb for hyper-parameter search for residual connection network,
run the following command in the command line: 
*****************
python main_rc.py
*****************

If you want to pass arguments to any of the python file, then follow the given format:

**********************************************************
python pthon_file.py --epochs 10 --lr 0.01 --batch_size 32
**********************************************************

*Note:* None of the argparse arguments are compulsory to run any of the above scripts.
The scripts will run with their default argument values in such cases. 
Please refer to the respective codes to know the arguments that can be passed. 
======================================================================================
======================================================================================