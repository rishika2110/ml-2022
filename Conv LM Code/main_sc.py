# Copyright (c) Facebook, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

from pathlib import Path
import argparse
import json
import math
import os
import random
import sys
import time
from tqdm import tqdm

import torch
from torch import nn
from torchvision.models import *
from torch.utils.data import DataLoader

import numpy as np
import warnings

from torchtext.datasets import WikiText2
from torchtext.data.utils import get_tokenizer
from torchtext.vocab import build_vocab_from_iterator

from model_ import conv_lm_model
from datautils import corp2tensor, stacker
import wandb

parser = argparse.ArgumentParser(description='Conv LM Training')
parser.add_argument('--data', type=Path, metavar='DIR',
                    help='path to dataset')
parser.add_argument('-a', '--arch', metavar='ARCH', default='custom_conv')
parser.add_argument('--workers', default=8, type=int, metavar='N',
                    help='number of data loader workers')
parser.add_argument('--epochs', default=3, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--batch-size', default=32, type=int, metavar='N',
                    help='mini-batch size')
parser.add_argument('--lr', default=0.05, type=float, metavar='LR',
                    help='base learning rate for weights')
parser.add_argument('--weight-decay', default=1e-4, type=float, metavar='W',
                    help='weight decay')

parser.add_argument('--seq-len', default=16, type=int, metavar='N',
                    help='Sequence length')
# parser.add_argument('--vocab-size', default=0, type=int, metavar='N',
#                     help='Vocabulary size')
parser.add_argument('--initial-layer-dim', default=1024, type=int, metavar='N',
                    help='Dimenstion of initial layer of network')
parser.add_argument('--num_layers', default=4, type=int, metavar='N',
                    help='Number of layers in network')
parser.add_argument('--layer_dim', default=[], type=list, metavar='N',
                    help="list of all dimensions of layers in network")

parser.add_argument('--print-freq', default=100, type=int, metavar='N',
                    help='print frequency')
parser.add_argument('--seed', default=1234, type=int,
                    help='seed for initializing training. ')
parser.add_argument('--gpu', default=0, type=int,
                    help='GPU id to use.')
parser.add_argument('--multiprocessing_distributed', action='store_true',
                    help='Use multi-processing distributed training to launch '
                         'N processes per node, which has N GPUs. This is the '
                         'fastest way to use PyTorch for either single node or '
                         'multi node data parallel training')
parser.add_argument('--checkpoint-dir', default='./experiments/', type=Path,
                    metavar='DIR', help='path to checkpoint directory')

sweep_config = {
    'method': 'grid',
    'metric': {
        'name': 'valid_loss',
        'goal': 'minimize'
    },
    'parameters': {
        'lr': {
            'values': [1e-3, 5e-3, 1e-2, 5e-3, 1e-1]
        },

        'batch_size': {
            'values': [16, 32]
        },

        'epochs': {
            'values': [3, 5]
        },

        'seq_len': {
            'values': [64, 256]
        },

        'initial_layer_dim' : {
            'values': [128, 256, 512, 1024]
        },

        'num_layers': {
            'values': [4, 8]
        },

    }
}


def main():
    args = parser.parse_args()

    with wandb.init(config=args, project="Convolution Language Modelling", entity="shitty"):

        config = wandb.config
        
        for num in range(config.num_layers):
            if num == 0:
                config.layer_dim.append(config.initial_layer_dim)
            if num < config.num_layers//2:
                config.layer_dim.append(1*config.layer_dim[num-1])
            if num > config.num_layers//2:
                 config.layer_dim.append(config.layer_dim[num-1]//1)

        print("="*75)
        print("CONFIG:", config)
        print("="*75)

        if args.seed is not None:
            os.environ["PYTHONHASHSEED"] = str(args.seed)
            random.seed(args.seed)
            np.random.seed(args.seed)
            torch.manual_seed(args.seed)
            torch.backends.cudnn.deterministic = True
            warnings.warn('You have chosen to seed training. '
                        'This will turn on the CUDNN deterministic setting, '
                        'which can slow down your training considerably! '
                        'You may see unexpected behavior when restarting '
                        'from checkpoints.')

        if args.multiprocessing_distributed and args.gpu is None:
            args.ngpus_per_node = torch.cuda.device_count()
            args.rank = 0
            args.dist_url = 'tcp://localhost:58472'
            args.world_size = args.ngpus_per_node
            torch.multiprocessing.spawn(main_worker, (args,), args.ngpus_per_node)
        elif args.gpu is not None:
            # Simply call main_worker function
            args.rank = 0
            main_worker(args.gpu, args, config)
        else:
            warnings.warn('Choose either 1 gpu or multiprocessing distributed and all gpus (None)')
            sys.exit()


def main_worker(gpu, args, config):
    if args.multiprocessing_distributed:
        args.rank += gpu
        torch.distributed.init_process_group(
            backend='nccl', init_method=args.dist_url,
            world_size=args.world_size, rank=args.rank)

    expt_name = "exp_bs"+str(config.batch_size)+"_sl"+str(config.seq_len)+"_ep"+str(config.epochs)+"_"+str(config.layer_dim)
    args.checkpoint_dir = args.checkpoint_dir / expt_name
    if args.rank == 0:
        args.checkpoint_dir.mkdir(parents=True, exist_ok=True)
        stats_file = open(args.checkpoint_dir / 'stats.txt', 'a', buffering=1)
        print(' '.join(sys.argv))
        print(' '.join(sys.argv), file=stats_file)
        losses_file = open(args.checkpoint_dir / 'loss_per_epoch.txt', 'a', buffering=1)
        print(' '.join(sys.argv), file=losses_file)

    torch.cuda.set_device(gpu)
    torch.backends.cudnn.benchmark = True

    # Creating utility objects (iterators, tokenizer and vocab) 
    train_iter, val_iter, test_iter = WikiText2()
    tknzr = get_tokenizer('basic_english')
    vocab = build_vocab_from_iterator(map(tknzr, train_iter), specials=['[UNK]'])
    vocab.set_default_index(vocab['[UNK]'])
    word2idx = vocab.get_stoi()
    idx2word = vocab.get_itos()
    
    config.vocab_size = len(vocab)

    model = conv_lm_model(config).cuda(gpu)
    if args.multiprocessing_distributed:
        model = nn.SyncBatchNorm.convert_sync_batchnorm(model)
    if args.multiprocessing_distributed:
        model = nn.parallel.DistributedDataParallel(model, device_ids=[gpu])
    print(model)

    # define loss function (criterion) and optimizer
    # criterion = nn.CosineSimilarity(dim=1).cuda(gpu)
    criterion = torch.nn.CrossEntropyLoss().cuda(gpu)


    # optimizer = torch.optim.SGD(optim_params, init_lr,
    #                             momentum=args.momentum,
    #                             weight_decay=args.weight_decay)
    optimizer = torch.optim.Adam(model.parameters())


    # automatically resume from checkpoint if it exists
    if (args.checkpoint_dir / 'checkpoint.pth').is_file():
        ckpt = torch.load(args.checkpoint_dir / 'checkpoint.pth',
                          map_location='cpu')
        start_epoch = ckpt['epoch']
        model.load_state_dict(ckpt['model'])
        optimizer.load_state_dict(ckpt['optimizer'])
    else:
        start_epoch = 0
    start_epoch = 0
    
    # dataset = torchvision.datasets.ImageFolder(args.data / 'train', Transform())
    

    # Creating dataset tensors
    train_data = corp2tensor(tknzr, vocab, mode='train')
    valid_data = corp2tensor(tknzr, vocab, mode='valid')
    test_data = corp2tensor(tknzr, vocab, mode='test')

    train_src_batched, train_trg_batched = stacker(train_data, config)
    valid_src_batched, valid_trg_batched = stacker(valid_data, config)
    test_src_batched, test_trg_batched = stacker(test_data, config)

    train_src_loader = DataLoader(train_src_batched, pin_memory=False)
    train_trg_loader = DataLoader(train_trg_batched, pin_memory=False)
    valid_src_loader = DataLoader(valid_src_batched, pin_memory=False)
    valid_trg_loader = DataLoader(valid_trg_batched, pin_memory=False)
    test_src_loader = DataLoader(test_src_batched, pin_memory=False)
    test_trg_loader = DataLoader(test_trg_batched, pin_memory=False)
    per_device_batch_size = args.batch_size
    sampler = None
    # if args.multiprocessing_distributed:
    #     train_sampler = torch.utils.data.distributed.DistributedSampler()
    #     assert args.batch_size % args.world_size == 0
    #     per_device_batch_size = args.batch_size // args.world_size
    
    # loader = torch.utils.data.DataLoader(
    #     dataset, batch_size=per_device_batch_size, num_workers=args.workers,
    #     pin_memory=True, sampler=sampler, shuffle= not args.multiprocessing_distributed)

    # Save encoder before training for feature plotting
    # if args.rank == 0:
    #     # save final model
    #     if args.multiprocessing_distributed:
    #         torch.save(model.module.state_dict(),
    #                 args.checkpoint_dir / '{}_{}ep.pth'.format(args.arch,'init'))
    #     else:
    #         torch.save(model.state_dict(),
    #                 args.checkpoint_dir / '{}_{}ep.pth'.format(args.arch,'init'))

    start_time = time.time()
    for epoch in range(start_epoch, config.epochs):
        if args.multiprocessing_distributed:
            sampler.set_epoch(epoch)
        epoch_loss = 0
        # switch to train mode
        model.train()

        for step, batch in tqdm(enumerate(zip(train_src_loader, train_trg_loader))):
            
            optimizer.zero_grad()

            src = batch[0].cuda().squeeze(0)
            trg = batch[1].cuda().squeeze(0)

            pred = model(src)
            # print(pred.shape, trg.shape)
            loss = criterion(pred.view(config.batch_size*config.seq_len, -1), trg.view(config.batch_size*config.seq_len))
            loss.backward()
            optimizer.step()

            epoch_loss += loss.item()
            if step % args.print_freq == 0:
                if args.rank == 0:
                    stats = dict(epoch=epoch+1, step=step,
                                 lr=optimizer.param_groups[0]['lr'],
                                 loss=loss.item(),
                                 time=int(time.time() - start_time))
                    print(json.dumps(stats))
                    print(json.dumps(stats), file=stats_file)
        # if args.rank == 0:
        #     # save checkpoint
        #     state = dict(epoch=epoch+1, model=model.state_dict(),
        #                  optimizer=optimizer.state_dict())
        #     torch.save(state, args.checkpoint_dir / 'checkpoint.pth')

        epoch_loss = epoch_loss/len(train_src_loader)
        if args.rank == 0:
            # Validation
            val_loss = Evaluate((valid_src_loader, valid_trg_loader), model, criterion, config)
            test_loss = Evaluate((test_src_loader, test_trg_loader), model, criterion, config)
            loss_per_epoch = dict(epoch=epoch+1, epoch_loss=epoch_loss, valid_loss=val_loss, test_loss=test_loss, train_ppl=math.exp(epoch_loss), val_ppl=math.exp(val_loss), test_ppl=math.exp(test_loss), time=int(time.time() - start_time))
            print(json.dumps(loss_per_epoch))
            print(json.dumps(loss_per_epoch), file=losses_file)
            wandb.log(dict(epoch=epoch, epoch_loss=epoch_loss, validation_loss=val_loss, test_loss=test_loss, tain_ppl=math.exp(epoch_loss), val_ppl=math.exp(val_loss), test_ppl=math.exp(test_loss), time=int(time.time() - start_time)))

        # Save encoder every 10 epochs for feature plotting
        # if (epoch+1)%10==0:
        #     if args.rank == 0:
        #         # save final model
        #         if args.multiprocessing_distributed:
        #             torch.save(model.module.encoder.state_dict(),
        #                     args.checkpoint_dir / '{}_{}ep.pth'.format(args.arch,str(epoch+1)))
        #         else:
        #             torch.save(model.encoder.state_dict(),
        #                     args.checkpoint_dir / '{}_{}ep.pth'.format(args.arch,str(epoch+1)))

    # if args.rank == 0:
    #     # save final model
    #     if args.multiprocessing_distributed:
    #         torch.save(model.module.state_dict(),
    #                 args.checkpoint_dir / '{}.pth'.format(args.arch))
    #     else:
    #         torch.save(model.state_dict(),
    #                 args.checkpoint_dir / '{}.pth'.format(args.arch))


def Evaluate(loaders, model, criterion, config):

    model.eval()
    eval_loss = 0
    with torch.no_grad():
        for _, batch in tqdm(enumerate(zip(loaders[0], loaders[1]))):
            source = batch[0].cuda().squeeze(0)
            target = batch[1].cuda().squeeze(0)
            outputs = model(source)
            batch_loss = criterion(outputs.view(config.batch_size*config.seq_len, -1), target.view(config.batch_size*config.seq_len))
            eval_loss += batch_loss.item()
        
        return eval_loss/len(loaders[0])


if __name__ == '__main__':
    
    sweep_id = wandb.sweep(sweep_config, project="Convolution Language Modelling")
    # main()

    wandb.agent(sweep_id, main)
