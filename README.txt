======================================================================================
======================================================================================
In the due course of the semester, we have worked on mainly two projects



===========================================================================
1. Convolutional Language Modelling
===========================================================================
The goal of this project is to develop self-supervised paradigms that
enable convolutional neural networks to learn information-rich language
representations.

Paper Link: https://drive.google.com/file/d/13rE6cLj0ZXydZzocmCUubG5o3E1h1nsz/view?usp=sharing

Code Link: https://gitlab.com/rishika2110/ml-2022/-/tree/main/Conv%20LM%20Code

GUI Link: https://gitlab.com/rishika2110/ml-2022/-/tree/main/Conv%20LM%20GUI
===========================================================================



===========================================================================
2. Enhancing Context Through Contrast
===========================================================================
The goal of this project was to utilize contrastive learning paradigms
from self-supervised learning to improve neural machine translation
performance by enhancing the context in the learned language
representations.

Proposal Link: https://preregister.science/papers_21neurips/18_paper.pdf

Result report and Code Link: https://drive.google.com/file/d/1Vgf4Pht0X70Bfyw172hyX5yEbMmtB1sa/view?usp=sharing

GUI Link: https://gitlab.com/rishika2110/ml-2022/-/tree/main/Trans%20GUI
======================================================================================
======================================================================================

Members (in order of descending contribution):
Rishika Bhagwatkar BT18ECE149
Khurshed Fitter    BT18ECE044
