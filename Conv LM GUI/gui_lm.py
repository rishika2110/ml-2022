# Import the required libraries
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from PIL import ImageTk, Image
from temp import sweep_lm_ppl, sweep_lm_loss, sweep_lm_rc_ppl, sweep_lm_rc_loss
from lm_train import train

# Create an instance of tkinter frame
win= Tk()

# Set the size of the tkinter window
win.geometry("800x350")

def train_lm(num_epochs, bs, lr):

    t = train(int(num_epochs), int(bs), float(lr))
    t.forward()
    
    # get_input() 
    # messagebox.showinfo("Message",output)

def get_input():
    global num_epochs, bs, lr
    #Import the required Libraries

    #Create an instance of Tkinter frame
    win= Tk()

    #Set the geometry of Tkinter frame
    win.geometry("700x350")

    def display_text():
        global num_epochs, bs, lr
        num_epochs = num_epochs.get()
        bs = bs.get()
        lr = lr.get()
        # label.configure(text=num_epochs+bs+lr)
        

    #Initialize a Label to display the User Input
    label=Label(win, text="Enter number of epochs:", font=("Courier 17 bold"))
    label.pack()

    #Create an Entry widget to accept User Input
    # label.configure(text="Enter number of epochs:")
    num_epochs = Entry(win, width= 50, textvariable="num_epochs")
    
    num_epochs.focus_set()
    
    num_epochs.pack()


    label=Label(win, text="Enter batch size:", font=("Courier 17 bold"))
    label.pack()
    # label.configure(text="Enter batch size:")
    bs = Entry(win, width= 50, textvariable="bs")
    
    bs.focus_set()
    
    bs.pack()
    

    label=Label(win, text="Enter lr:", font=("Courier 17 bold"))
    label.pack()
    # label.configure(text="Enter lr:")
    lr = Entry(win, width= 50, textvariable="lr")
    
    lr.focus_set()
    
    lr.pack()
    

    # hparams = "Epochs: " + str(num_epochs) + "Batch size: " + str(bs) + "Learning Rate: " + str(lr)
    # messagebox.showinfo("Message", hparams)

    #Create a Button to validate Entry Widget
    ttk.Button(win, text= "Submit parameters", width= 20, command= display_text).pack(pady=20)
    b = ttk.Button(win, text= "Begin Training!",width= 30, command=lambda: train_lm(num_epochs, bs, lr))
    b.pack(pady=20)

    win.mainloop()

def sweep_lm_ppl_graph():
    sweep_lm_ppl()

def sweep_lm_loss_graph():
    sweep_lm_loss()

def sweep_lm_ppl_rc_graph():
    sweep_lm_rc_ppl()

def sweep_lm_loss_rc_graph():
    sweep_lm_rc_loss()

# Add an optional Label widget
Label(win, text= "Please select the appropiate buttons for repestive functionalities of Language Modelling task", font= ('Times 15')).pack(pady= 30)

# Create a Button to display the message
ttk.Button(win, text= "Train using customised hyper-parameters", width = 100, command=get_input).pack(pady= 7)
ttk.Button(win, text= "Sweep loss graphs (Without Rsidual connections network)", width = 100, command=sweep_lm_loss_graph).pack(pady= 7)
ttk.Button(win, text= "Sweep perplexity graphs (Without Rsidual connections network)", width = 100, command=sweep_lm_ppl_graph).pack(pady= 7)
ttk.Button(win, text= "Sweep loss graphs (With Rsidual connections network)", width = 100, command=sweep_lm_loss_rc_graph).pack(pady= 7)
ttk.Button(win, text= "Sweep perplexity graphs (With Rsidual connections network)", width = 100, command=sweep_lm_ppl_rc_graph).pack(pady= 7)



win.mainloop()