import math
import torch
from torchtext.datasets import WikiText2

# A small data processor
def corp2tensor(tknzr, vocab, mode='train'):

    raw_iter = WikiText2(split=mode)    
    crps = []
    for sntnc in raw_iter:
        tknzd = tknzr(sntnc)
        vocbd = vocab(tknzd)
        if len(vocbd):
            crps.extend(vocbd)
    return torch.tensor(crps)


def stacker(unbatched, config):

    block_size = config.batch_size * config.seq_len
    num_blocks = unbatched.shape[0] // block_size
    cutoff_idx = num_blocks * block_size
    trimmed_src = unbatched[:cutoff_idx]
    trimmed_trg = unbatched[1:cutoff_idx+1]
    source_stack = trimmed_src.view(num_blocks, config.batch_size, int(math.sqrt(config.seq_len)), int(math.sqrt(config.seq_len)))
    target_stack = trimmed_trg.view(num_blocks, config.batch_size, int(math.sqrt(config.seq_len)), int(math.sqrt(config.seq_len)))
    
    return source_stack, target_stack


