class Config:
    batch_size = 32
    seq_len = 64
    max_epochs = 100
    vocab_size = None
    layer_dim = [512, 1024, 2048, 1024, 512]

