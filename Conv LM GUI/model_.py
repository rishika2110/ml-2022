import torch
import torch.nn as nn
import torch.nn.functional as F

class conv_lm_model(nn.Module):
    def __init__(self, config):
        super().__init__()

        self.layers = []
        self.embedding = nn.Embedding(num_embeddings=config.vocab_size, embedding_dim=config.layer_dim[0])
        for i in range(config.num_layers-1):
            self.layers.append(nn.Sequential(nn.Conv2d(in_channels=config.layer_dim[i], out_channels=config.layer_dim[i+1], kernel_size=3, padding=1), 
                                        nn.BatchNorm2d(num_features=config.layer_dim[i+1])))
        self.layers = nn.ModuleList(self.layers)
        self.fc = nn.Linear(in_features=config.layer_dim[-1], out_features=config.vocab_size)

    def forward(self, x):
        '''
        x = [b, sqrt(seq_len), sqrt(seq_len), emb_dim]
        out = [b, sqrt(seq_len), sqrt(seq_len), vocab_size]
        '''
         
        x = self.embedding(x).permute(0, 3, 1, 2)
        xprev = x
        for layer in self.layers:
            x = layer(x)
            x = F.relu(x) + xprev
            xprev = x
        out = self.fc(x.permute(0, 2, 3, 1))
        # out = F.softmax(x, -1)

        return out