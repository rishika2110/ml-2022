import matplotlib.pyplot as plt
from PIL import Image


## Helping functions for plotting in GUI 

def templot():
    add = ["translation gui/Seq2Seq-2.jpeg",
    "translation gui/Seq2Seq_with_Attention.jpeg",
    "translation gui\Conv_Seq2Seq.jpeg",
    "translation gui/Transformer.jpeg" ]

    f, axarr = plt.subplots(2,2)

    image_datas = []
    for i in range(4):
        image_datas.append(Image.open(add[i]))

    axarr[0,0].imshow(image_datas[0])
    axarr[0,1].imshow(image_datas[1])
    axarr[1,0].imshow(image_datas[2])
    axarr[1,1].imshow(image_datas[3])

    title_fontsize=20
    axarr[0,0].set_title("Seq2Seq", fontsize=title_fontsize)
    axarr[0,1].set_title("Seq2Seq with Attention", fontsize=title_fontsize)
    axarr[1,0].set_title("Conv Seq2Seq", fontsize=title_fontsize)
    axarr[1,1].set_title("Transformer", fontsize=title_fontsize)

    axarr[0,0].axis('off')
    axarr[0,1].axis('off')
    axarr[1,0].axis('off')
    axarr[1,1].axis('off')

    axarr[0,0].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[0,1].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[1,0].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[1,1].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)

    plt.tight_layout()
    plt.show()


def sweep_ce_plot():
    add = ["translation gui\Iter_loss_ce.png",
    "translation gui\epoch_loss_ce.png" ]

    f, axarr = plt.subplots(1,2)

    image_datas = []
    for i in range(2):
        image_datas.append(Image.open(add[i]))

    axarr[0].imshow(image_datas[0])
    axarr[1].imshow(image_datas[1])

    title_fontsize=20
    axarr[0].set_title("Iteration Loss", fontsize=title_fontsize)
    axarr[1].set_title("Epoch Loss", fontsize=title_fontsize)

    axarr[0].axis('off')
    axarr[1].axis('off')

    axarr[0].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[1].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)

    plt.tight_layout()
    plt.show()


def sweep_trans_plot():
    add = ["translation gui/iter_loss_translation.png",
    "translation gui/epoch_loss_translation.png" ]

    f, axarr = plt.subplots(1,2)

    image_datas = []
    for i in range(2):
        image_datas.append(Image.open(add[i]))

    axarr[0].imshow(image_datas[0])
    axarr[1].imshow(image_datas[1])

    title_fontsize=20
    axarr[0].set_title("Iteration Loss", fontsize=title_fontsize)
    axarr[1].set_title("Epoch Loss", fontsize=title_fontsize)

    axarr[0].axis('off')
    axarr[1].axis('off')

    axarr[0].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[1].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)

    plt.tight_layout()
    plt.show()


def sweep_lm_ppl():
    add = ["language modelling gui/train_ppl_norm.png",
    "language modelling gui/val_ppl_norm.png",
    "language modelling gui/test_ppl_norm.png" ]

    f, axarr = plt.subplots(1,3)

    image_datas = []
    for i in range(3):
        image_datas.append(Image.open(add[i]))

    axarr[0].imshow(image_datas[0])
    axarr[1].imshow(image_datas[1])
    axarr[2].imshow(image_datas[2])

    title_fontsize=20
    axarr[0].set_title("Train Perplexity", fontsize=title_fontsize)
    axarr[1].set_title("Validation Perplexity", fontsize=title_fontsize)
    axarr[2].set_title("Test Perplexity", fontsize=title_fontsize)

    axarr[0].axis('off')
    axarr[1].axis('off')
    axarr[2].axis('off')

    axarr[0].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[1].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[2].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)

    plt.tight_layout()
    plt.show()


def sweep_lm_loss():
    add = ["language modelling gui/epoch_loss_norm.png",
    "language modelling gui/val_loss_norm.png" ]

    f, axarr = plt.subplots(1,2)

    image_datas = []
    for i in range(2):
        image_datas.append(Image.open(add[i]))

    axarr[0].imshow(image_datas[0])
    axarr[1].imshow(image_datas[1])

    title_fontsize=20
    axarr[0].set_title("Train Loss", fontsize=title_fontsize)
    axarr[1].set_title("Validation Loss", fontsize=title_fontsize)

    axarr[0].axis('off')
    axarr[1].axis('off')

    axarr[0].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[1].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)

    plt.tight_layout()
    plt.show()



def sweep_lm_rc_ppl():
    add = ["language modelling gui/train_ppl_rc.jpeg",
    "language modelling gui/val_ppl_rc.jpeg",
    "language modelling gui/test_ppl_rc.jpeg" ]

    f, axarr = plt.subplots(1,3)

    image_datas = []
    for i in range(3):
        image_datas.append(Image.open(add[i]))

    axarr[0].imshow(image_datas[0])
    axarr[1].imshow(image_datas[1])
    axarr[2].imshow(image_datas[2])

    title_fontsize=20
    axarr[0].set_title("Train Perplexity", fontsize=title_fontsize)
    axarr[1].set_title("Validation Perplexity", fontsize=title_fontsize)
    axarr[2].set_title("Test Perplexity", fontsize=title_fontsize)

    axarr[0].axis('off')
    axarr[1].axis('off')
    axarr[2].axis('off')

    axarr[0].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[1].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[2].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)

    plt.tight_layout()
    plt.show()


def sweep_lm_rc_loss():
    add = ["language modelling gui/epoch_loss_rc.jpeg",
    "language modelling gui/val_loss_rc.jpeg" ]

    f, axarr = plt.subplots(1,2)

    image_datas = []
    for i in range(2):
        image_datas.append(Image.open(add[i]))

    axarr[0].imshow(image_datas[0])
    axarr[1].imshow(image_datas[1])

    title_fontsize=20
    axarr[0].set_title("Train Loss", fontsize=title_fontsize)
    axarr[1].set_title("Validation Loss", fontsize=title_fontsize)

    axarr[0].axis('off')
    axarr[1].axis('off')

    axarr[0].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)
    axarr[1].tick_params(left = False, right = False , labelleft = False ,
                    labelbottom = False, bottom = False)

    plt.tight_layout()
    plt.show()



