======================================================================================
======================================================================================
  This folder contains files to run GUI for the Convolution Language Modelling task  
======================================================================================


Important Contents:
======================================================================================
1. language modelling gui: Folder containing graphs of both with and without residual
   connections loss and perplexity graphs along.
2. gui_lm.py: Python file to run the gui for language modelling task.
======================================================================================


Functionalities incorporated in the GUI:
======================================================================================
1. Train using customised hyper-parameters
2. Display hyper-parameter sweep loss graphs for without residual connections network
3. Display hyper-parameter sweep perplexity graphs for without residual connections network
4. Display hyper-parameter sweep loss graphs for with residual connections network
5. Display hyper-parameter sweep perplexity graphs for with residual connections network
======================================================================================
======================================================================================

Run the below command in the terminal to launch the GUI after changing the current directory:
*******************
python gui_lm.py
*******************
