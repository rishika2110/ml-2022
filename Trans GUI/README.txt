======================================================================================
======================================================================================
           This folder contains files to run GUI for the Translation task  
======================================================================================


Important Contents:
======================================================================================
1. translation gui: Folder containing graphs of hyper-parameter sweep and our 
   transformer based trained model
2. gui_trans.py: Python file to run the gui for translation task.
======================================================================================


Functionalities incorporated in the GUI:
======================================================================================
1. Using pretrained weights to translate German to English.
2. Display hyper-parameter sweep loss graphs for context enhancement step.
3. Display hyper-parameter sweep loss graphs for translation step.
4. Display learning curves for optimal models.
======================================================================================
======================================================================================

Run the below command in the terminal to launch the GUI after changing the current directory:
*******************
python gui_trans.py
*******************
