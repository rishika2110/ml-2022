import math
import numpy as np
import random
import spacy
import torch
import torch.nn as nn
from torchtext.data import Field
from torchtext.datasets import Multi30k
# from torchtext.data import Field, BucketIterator
from torchtext.data.metrics import bleu_score

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

SEED = 4444

random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True

de_model = spacy.load("de_core_news_sm")
en_model = spacy.load("en_core_web_sm")

def de_tokenizer(sentence):
    return [token.text for token in de_model.tokenizer(sentence)]


def en_tokenizer(sentence):
    return [token.text for token in en_model.tokenizer(sentence)]


def ipTensor(sentence, src_field):
    if isinstance(sentence, list):
        tokens = [src_field.init_token] + [token.lower() for token in sentence] + [src_field.eos_token]
    else:
        tokens = [src_field.init_token] + [token.text.lower() for token in de_tokenizer(sentence)] + [src_field.eos_token]
    seq_len = len(tokens)
    ip_tensor = torch.LongTensor([src_field.vocab.stoi[token] for token in tokens]).to(device)
    return ip_tensor.view(1, seq_len)


class MultiHead_Attn_Layer(nn.Module):
    def __init__(self, hidden_dim, n_heads, dropout):
        super(MultiHead_Attn_Layer, self).__init__()
        self.hidden_dim = hidden_dim
        self.n_heads = n_heads
        self.head_dim = hidden_dim // n_heads
        self.fc_Q = nn.Linear(hidden_dim, hidden_dim)
        self.fc_K = nn.Linear(hidden_dim, hidden_dim)
        self.fc_V = nn.Linear(hidden_dim, hidden_dim)
        self.fc_O = nn.Linear(hidden_dim, hidden_dim)
        self.scale = math.sqrt(self.head_dim)
        self.dropout = nn.Dropout(dropout)

    def forward(self, query, key, value, mask=None):                                    # [query] = [batch_size, query_len, hidden_dim] [key] = [batch_size, key_len, hidden_dim] [value] = [batch_Size, value_len, hidden_dim]
        batch_size = query.shape[0]                                           
        Q = self.fc_Q(query)                                                            # [Q] = [batch_size, query_len, hidden_dim]   
        K = self.fc_K(key)                                                              # [K] = [batch_size, key_len, hidden_dim]
        V = self.fc_V(value)                                                            # [V] = [batch_size, value_len, hidden_dim]
        Q = Q.view(batch_size, -1, self.n_heads, self.head_dim).permute(0, 2, 1, 3)     # [Q] = [batch_size, num_heads, query_len, head_dim]
        K = K.view(batch_size, -1, self.n_heads, self.head_dim).permute(0, 2, 1, 3)     # [K] = [batch_size, num_heads, key_len, head_dim]
        V = V.view(batch_size, -1, self.n_heads, self.head_dim).permute(0, 2, 1, 3)     # [V] = [batch_size, num_heads, value_len, head_dim]
        energy = torch.matmul(Q, K.permute(0, 1, 3, 2)) / self.scale
        if mask is not None:                                                            # [energy] = [batch_size, num_heads, query_len, key_len]  
            energy = energy.masked_fill(mask == False, -1e10)                               
        attention = torch.softmax(energy, dim = -1)                                     # [attention] = [batch_size, num_heads, query_len, key_len]
        x = torch.matmul(self.dropout(attention), V)                                    # [x] = [batch_size, num_heads, query_len, head_dim]
        x = x.permute(0, 2, 1, 3).contiguous()                                          # [x] = [batch_size, query_len, num_heads, head_dim]
        # Can avoid contiguous() if we use .reshape instead of .view in the next line
        out = self.fc_O(x.view(batch_size, -1, self.hidden_dim))                        # [out] = [batch_size, query_len, hidden_dim]   
        return out, attention


class Postn_Feed_Fwrd(nn.Module):
    def __init__(self, hidden_dim, pff_dim, dropout):
        super(Postn_Feed_Fwrd, self).__init__()
        self.fc1 = nn.Linear(hidden_dim, pff_dim)
        self.fc2 = nn.Linear(pff_dim, hidden_dim)
        self.dropout = nn.Dropout(dropout)

    def forward(self, input):                                                   # input = [batch_size, seq_len, hidden_dim]
        out = torch.relu(self.fc1(input))                                       # out = [batch_size, seq_len, pff_dim]
        out = self.fc2(self.dropout(out))                                       # out = [batch_size, seq_len, hidden_dim] 
        return out


class Encoder_Layer(nn.Module):
    def __init__(self, hidden_dim, n_heads, pff_dim, dropout):
        super(Encoder_Layer, self).__init__()
        self.self_attn = MultiHead_Attn_Layer(hidden_dim, n_heads, dropout)    
        self.pff = Postn_Feed_Fwrd(hidden_dim, pff_dim, dropout)
        self.attn_norm = nn.LayerNorm(hidden_dim)
        self.pff_norm = nn.LayerNorm(hidden_dim)
        self.dropout = nn.Dropout(dropout)

    def forward(self, src, src_mask):                                           # src = [batch_size, src_len, hidden_dim]  src_mask = [batch_size, src_len]
        attn_out, _ = self.self_attn(src, src, src, src_mask)                   # attn_out = [batch_size, src_len, hidden_dim]
        inter_out = self.attn_norm(self.dropout(attn_out) + src)                # inter_out = [batch_size, src_len, hidden_dim]
        pff_out = self.pff(inter_out)                                           # pff_out = [batch_size, src_len, hidden_dim]
        out = self.pff_norm(self.dropout(pff_out) + inter_out)                  # out = [batch_size, src_len, hidden_dim]
        return out


class Encoder(nn.Module):
    def __init__(self, tok_vocab_size, pos_vocab_size, hidden_dim, enc_heads, enc_pff_dim, num_layers, enc_dropout):
        super(Encoder, self).__init__()
        self.tok_embedding = nn.Embedding(tok_vocab_size, hidden_dim)
        self.pos_embedding = nn.Embedding(pos_vocab_size, hidden_dim)
        self.enc_layers = nn.ModuleList([Encoder_Layer(hidden_dim, enc_heads, enc_pff_dim, enc_dropout) for i in range(num_layers)])
        self.scale = math.sqrt(hidden_dim)
        self.dropout = nn.Dropout(enc_dropout)

    def forward(self, src, src_mask):                                                       # src = [batch_size, src_len] src_mask = [batch_size, src_mask]
        batch_size = src.shape[0]                     
        src_len = src.shape[1]
        tok_embed = self.tok_embedding(src)                                                 # tok_embed = [batch_size, src_len, hidden_dim]
        pos_tensor = torch.arange(0, src_len).unsqueeze(0).repeat(batch_size, 1).to(device) # pos_tensor = [batch_size, src_len]
        pos_embed = self.pos_embedding(pos_tensor)                                          # pos_embed = [batch_size, src_len, hidden_dim]   
        enc_embed = self.dropout(tok_embed * self.scale + pos_embed)                        # enc_embed = [batch_size, src_len, hidden_dim] 
        enc_state = enc_embed           
        for enc_layer in self.enc_layers:
            enc_state = enc_layer(enc_state, src_mask)                                      # enc_state = [batch_size, src_len, hidden_dim]
        return enc_state  


class Decoder_Layer(nn.Module):
    def __init__(self, hidden_dim, n_heads, pff_dim, dropout):
        super(Decoder_Layer, self).__init__()
        self.self_attn = MultiHead_Attn_Layer(hidden_dim, n_heads, dropout)
        self.cross_attn = MultiHead_Attn_Layer(hidden_dim, n_heads, dropout)
        self.pff = Postn_Feed_Fwrd(hidden_dim, pff_dim, dropout)
        self.attn_norm1 = nn.LayerNorm(hidden_dim)
        self.attn_norm2 = nn.LayerNorm(hidden_dim)
        self.pff_norm = nn.LayerNorm(hidden_dim)
        self.dropout = nn.Dropout(dropout)

    def forward(self, trg, trg_mask, enc_out, src_mask):                            # trg = [batch_size, trg_len, hidden_dim] trg_mask = [batch_size, trg_len] enc_out = [batch_size, src_len, hidden_dim] src_mask = [batch_size, src_len]
        sattn_out, _ = self.self_attn(trg, trg, trg, trg_mask)                      # satten_out = [batch_size, trg_len, hidden_dim]
        inter_out1 = self.attn_norm1(self.dropout(sattn_out) + trg)                 # inter_out1 = [batch_size, trg_len, hidden_dim]  
        cattn_out, attn = self.cross_attn(inter_out1, enc_out, enc_out, src_mask)   # cattn_out = [batch_size, trg_len, hidden_dim] attn = [batch_size, num_heads, query_len, key_len]
        inter_out2 = self.attn_norm2(self.dropout(cattn_out) + inter_out1)          # inter_out2 = [batch_size, trg_len, hidden_dim]
        pff_out = self.pff(inter_out2)                                              # pff_out = [batch_size, trg_len, hidden_dim]
        out = self.pff_norm(self.dropout(pff_out) + inter_out2)                     # out = [batch_size, trg_len, hidden_dim]
        return out, attn


class Decoder(nn.Module):
    def __init__(self, tok_vocab_size, pos_vocab_size, hidden_dim, dec_heads, dec_pff_dim, num_layers, dec_dropout):
        super(Decoder, self).__init__()
        self.tok_embedding = nn.Embedding(tok_vocab_size, hidden_dim)
        self.pos_embedding = nn.Embedding(pos_vocab_size, hidden_dim)
        self.dec_layers = nn.ModuleList([Decoder_Layer(hidden_dim, dec_heads, dec_pff_dim, dec_dropout) for i in range(num_layers)])
        self.fc = nn.Linear(hidden_dim, tok_vocab_size)
        self.scale = math.sqrt(hidden_dim)
        self.dropout = nn.Dropout(dec_dropout)

    def forward(self, trg, trg_mask, enc_out, src_mask):                                    # trg = [batch_size, trg_len] trg_mask = [batch_size, trg_len] enc_out = [] src_mask = []
        batch_size = trg.shape[0] 
        trg_len = trg.shape[1]
        tok_embed = self.tok_embedding(trg)                                                 # tok_embed = [batch_size, trg_len, hidden_dim]
        pos_tensor = torch.arange(0, trg_len).unsqueeze(0).repeat(batch_size, 1).to(device) # pos_tensor = [batch_size, trg_len]
        pos_embed = self.pos_embedding(pos_tensor)                                          # pos_embed = [batch_size, trg_len, hidden_dim]
        dec_embed = self.dropout(tok_embed * self.scale + pos_embed)                        # dec_embed = [batch_size, trg_len, hidden_dim]
        dec_state = dec_embed
        for dec_layer in self.dec_layers:
            dec_state, attention = dec_layer(dec_state, trg_mask, enc_out, src_mask)        # dec_state = [batch_size, trg_len, hidden_dim] attention = [batch_size, num_heads, query_len, key_len]
        out = self.fc(dec_state)                                                            # out = [batch_size, tok_vocab_size]
        return out, attention


class Seq2Seq(nn.Module):
    def __init__(self, encoder, decoder, src_padding_idx, trg_padding_idx):
        super(Seq2Seq, self).__init__()
        self.encoder = encoder
        self.decoder = decoder
        self.src_padding_idx = src_padding_idx
        self.trg_padding_idx = trg_padding_idx

    def make_src_mask(self, src):                                                       # src = [batch_size, src_len]
        src_mask = (src != self.src_padding_idx).unsqueeze(1).unsqueeze(2).to(device)   # src_mask = [batch_size, 1, 1, src_len]
        return src_mask

    def make_trg_mask(self, trg):                                                       # trg = [batch_size, trg_len]                  
        trg_len = trg.shape[1] 
        pad_mask = (trg != self.trg_padding_idx).unsqueeze(1).unsqueeze(2).to(device)   # pad_mask = [batch_size, 1, 1, trg_len]
        sub_mask = torch.tril(torch.ones((trg_len, trg_len), device = device)).bool()   # sub_mask = [trg_len, trg_len]
        trg_mask = pad_mask & sub_mask                                                  # trg_mask = [batch_size, 1, trg_len, trg_len]
        return trg_mask
    
    def forward(self, src, trg):                                                        # src = [batch_size, src_len] trg = [batch_size, trg_len]   
        src_mask = self.make_src_mask(src)
        trg_mask = self.make_trg_mask(trg)
        enc_out = self.encoder(src, src_mask)
        output, attention = self.decoder(trg, trg_mask, enc_out, src_mask)
        return output, attention


def Translate(src_sentence, src_field, trg_field, model):
    ip_tensor = ipTensor(src_sentence, src_field)
    max_len = 4 * ip_tensor.shape[1]
    src_mask = model.make_src_mask(ip_tensor)
    with torch.no_grad():
        enc_out = model.encoder(ip_tensor, src_mask)
    sos_id = trg_field.vocab.stoi[trg_field.init_token]
    eos_id = trg_field.vocab.stoi[trg_field.eos_token]
    predicts = [sos_id]
    for i in range(max_len):
        input = torch.LongTensor(predicts).unsqueeze(0).to(device)
        trg_mask = model.make_trg_mask(input)
        with torch.no_grad():
            output, attention = model.decoder(input, trg_mask, enc_out, src_mask)
        predict_tok = output.argmax(2)[:, -1].item()
        predicts.append(predict_tok)
        if predict_tok == eos_id:
            break
    sentence = [trg_field.vocab.itos[id] for id in predicts[1:]]
    return sentence, attention


def inference(ind):

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    SOURCE_Field = Field(eos_token = '<src_eos>', init_token = '<src_sos>', lower = True, tokenize = de_tokenizer, batch_first = True)

    TARGET_Field = Field(eos_token = '<trg_eos>', init_token = '<trg_sos>', lower = True, tokenize = en_tokenizer, batch_first = True)
    
    train_data, valid_data, test_data = Multi30k.splits(exts = ('.de', '.en'), fields = (SOURCE_Field, TARGET_Field))
    SOURCE_Field.build_vocab(train_data, min_freq = 2)
    TARGET_Field.build_vocab(train_data, min_freq = 2)

    SRC_VOCAB_SIZE = len(SOURCE_Field.vocab) + 2
    TRG_VOCAB_SIZE = len(TARGET_Field.vocab)
    HIDDEN_DIM = 256
    ENC_PFF_DIM = 512
    DEC_PFF_DIM = 512
    ENC_HEADS = 8
    DEC_HEADS = 8
    ENC_DROPOUT = 0.1
    DEC_DROPOUT = 0.1
    ENC_NUM_LAYERS = 3
    DEC_NUM_LAYERS = 3
    MAX_LEN = 100

    src_padding_idx = SOURCE_Field.vocab.stoi[SOURCE_Field.pad_token]
    trg_padding_idx = TARGET_Field.vocab.stoi[TARGET_Field.pad_token]

    encoder = Encoder(SRC_VOCAB_SIZE, MAX_LEN, HIDDEN_DIM, ENC_HEADS, ENC_PFF_DIM, ENC_NUM_LAYERS, ENC_DROPOUT).to(device)
    decoder = Decoder(TRG_VOCAB_SIZE, MAX_LEN, HIDDEN_DIM, DEC_HEADS, DEC_PFF_DIM, DEC_NUM_LAYERS, DEC_DROPOUT).to(device)
    seq2seq = Seq2Seq(encoder, decoder, src_padding_idx, trg_padding_idx).to(device)

    MODEL_STORE_PATH = "translation gui/Transformer.pth"

    seq2seq.load_state_dict(torch.load(MODEL_STORE_PATH, map_location=device))

    seq2seq.eval()

    example = test_data.examples[ind]
    src_sentence = example.src
    trg_sentence = example.trg
    # print("German Sentence: ", ' '.join(src_sentence))
    translation, attention = Translate(src_sentence, SOURCE_Field, TARGET_Field, seq2seq)
    # print("Predicted Translation: ", ' '.join(translation[:-1]))
    # print("Actual Translation: ", ' '.join(trg_sentence))

    return src_sentence, translation[:-1], trg_sentence

    
