from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from temp import templot, sweep_ce_plot, sweep_trans_plot
from trans_eval import inference

# Create an instance of tkinter frame
fr_wind = Tk()

# Set the size of the tkinter window
fr_wind.geometry("700x350")

def get_input():
    global input_seq
    #Import the required Libraries

    #Create an instance of Tkinter frame
    win= Tk()

    #Set the geometry of Tkinter frame
    win.geometry("800x250")

    def display_text():
        global input_seq
        string= input_seq.get()
        # output = "Translation:\n_123_"
        ip, op, org = inference(int(string))
        ip = "German sentence: "+ " ".join(ip)
        op = "Translation in English: " + " ".join(op)
        org = "Original transaltion: " + " ".join(org)
        Label(win, text=ip, font=("Times 12")).pack(pady=5)
        Label(win, text=op, font=("Times 12")).pack(pady=5)
        Label(win, text=org, font=("Times 12")).pack(pady=5)
        

    #Initialize a Label to display the User Input
    label=Label(win, text="", font=("Courier 17 bold"))
    label.pack()

    #Create an Entry widget to accept User Input
    label.configure(text="Enter any number:")
    input_seq= Entry(win, width= 50, textvariable="input_seq")
    
    input_seq.focus_set()
    
    input_seq.pack()
    # messagebox.showinfo("Message",input_seq)

    #Create a Button to validate Entry Widget
    ttk.Button(win, text= "Translate",width= 30, command= display_text).pack(pady=20)

    win.mainloop()

# Define a function to show the popup message
def show_translation():
    # username= Entry(fr_wind, textvariable="username")  
    # username.pack()
    # output = "123" 
    get_input() 
    # messagebox.showinfo("Message",output)

def sweep_ce_loss_graph():
    sweep_ce_plot()

def optimal_loss_img():
    templot()

def sweep_trans_loss_graph():
	sweep_trans_plot()
    

# Add an optional Label widget
Label(fr_wind, text= "Please select the appropiate buttons for repestive functionalities of translation", font= ('Times 15')).pack(pady= 30)

# Create a Button to display the message
ttk.Button(fr_wind, text= "Use Pretrained model to Translate German to English",width= 100, command=show_translation).pack(pady= 5)
ttk.Button(fr_wind, text= "Context Enhancement step Sweep loss graphs", width= 100, command=sweep_ce_loss_graph).pack(pady= 5)
ttk.Button(fr_wind, text= "Translation step Sweep loss graphs", width= 100, command=sweep_trans_loss_graph).pack(pady= 5)
ttk.Button(fr_wind, text= "Optimal loss graphs", width= 100, command=optimal_loss_img).pack(pady= 5)

fr_wind.mainloop()

